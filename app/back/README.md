
# API

App Laravel

## Getting started

```
npm install
npm run production
composer update
composer instalphp artisan serve
```

## Env

DATABASE_URL
DB_HOST
DB_PORT
DB_DATABASE
DB_USERNAME
DB_PASSWORD
DB_SOCKET

RABBITMQ_HOST
RABBITMQ_PORT
RABBITMQ_USER
RABBITMQ_PASSWORD
RABBITMQ_VHOST

ELASTICSEARCH_HOST
ELASTICSEARCH_PORT
ELASTICSEARCH_SCHEME
ELASTICSEARCH_USER
ELASTICSEARCH_PASS