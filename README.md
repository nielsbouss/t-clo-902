# T-CLO-902

## Amazon Web Services cluster

To deploy the project on AWS you need to setup aws cli and create the cluster with
```
eksctl create cluster -f cluster.yaml
```

When the cluster is created, you can connect to the cluster
```
aws eks --region eu-west-1 update-kubeconfig --name your-eks-cluster
```

## Setup cluster

To deploy the charts, you first need to setup your cluster

Activate Ingress if you are on docker desktop
```
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v0.41.2/deploy/static/provider/cloud/deploy.yaml
```

Activate Ingress if you are on AWS
```
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v0.48.1/deploy/static/provider/aws/deploy.yaml
```

```
# Elasticsearch
kubectl apply -f https://download.elastic.co/downloads/eck/1.6.0/all-in-one.yaml
# RabbitMQ
kubectl apply -f https://github.com/rabbitmq/cluster-operator/releases/latest/download/cluster-operator.yml
```

## Install

To install helm chart
```
helm install clo902 helm
```

## Access

If you run locally can access the app on *http://localhost/*

If you run on AWS, you can get the ip with
```
kubectl describe ingress
```

___

## ElasticSearch

Monitor your elasticsearch
```
kubectl get elasticsearch
```

Get password
```
kubectl get secret clo902-elasticsearch-es-elastic-user -o go-template='{{.data.elastic | base64decode}}'
```

## RabbitMQ

Get username and password
```
kubectl get secret clo902-rabbitmq-default-user -o jsonpath='{.data.username}' | base64 --decode
kubectl get secret clo902-rabbitmq-default-user -o jsonpath='{.data.password}' | base64 --decode
```

You can connect to the RabbitMQ interface on http://localhost:15672

## Debug DNS

Create the curl pod
```
kubectl run curl --image=radial/busyboxplus:curl -i --tty
```

Connect to the pod
```
kubectl attach curl -c curl -i -t
```
